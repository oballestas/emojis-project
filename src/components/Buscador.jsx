export function Buscador ({valorInput,onInputChange}) {
    return (
     <div className="row">
        <div className="col-6"></div>
        <div className="col-6 mb-3">
            <input 
            value={valorInput}
            onChange={onInputChange}
            type="text"
            className="form-control"
            placeholder="Ingrese el nombre de su emoji"
            />
        </div>
    </div>   
    )
} 