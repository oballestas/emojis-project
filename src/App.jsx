import './App.css';
import { TituloEncabezado } from './components/Titulo';
import { Buscador } from './components/Buscador';
import { EmojiList } from './components/EmojiList';
//import baseDatos from './database/emoji-list.json';
import {useState,useEffect} from 'react';
import Pagination from "react-js-pagination";
import 'bootstrap/dist/css/bootstrap.min.css';


let LIMIT=24;
let URL = `http://localhost:3001/emojis?_limit=${LIMIT}`;


function App() {
  let [emojis,setEmojis]=useState([]);
  let [busqueda,setBusqueda]=useState("");
  let [pagina,setPagina]=useState(1);
  let [emojisCompletos,setEmojisCompletos]=useState([]);

  let emojisFilter=emojis.filter((emoji)=>{
    if (emoji.title.toLowerCase().includes(busqueda.toLowerCase())){
      return emoji;
    }
  });

  //Se obitnen todos los datos de la API
 
  /** 
  useEffect( ()=> {
    fetch(`http://localhost:3001/emojis`)
      .then(resp=>resp.json())
      .then(datos=>{
        setEmojisCompletos(datos);
      });
  },[]);
  */

  // Se hace la busqueda de forma local
  
  useEffect( ()=> {
    /*
    async function obtenerDatos (){
      let respuesta=await fetch(URL);
      let datos= await respuesta.json();
      setEmojis(datos);
    };
    obtenerDatos();
    */

    fetch(`${URL}&_page=${pagina}`)
      .then(resp=>resp.json())
      .then(datos=>{
        setEmojis(datos);
      });
      window.scrollTo(0,0);
  },[pagina]);


  function actualizarInput(evento){
    let entradaTeclado= evento.target.value;
    setBusqueda(entradaTeclado);
    console.log(entradaTeclado);
  }

  function handlePageChange(newPage) {
    console.log(`You're in the page ${newPage}`)
    setPagina(newPage);
  }
  
  return (
    <div className="container">
      <TituloEncabezado/>
      <Buscador valorInput={busqueda} onInputChange={actualizarInput}/>
      <EmojiList datos={busqueda? emojisFilter:emojis}/>

      <div className='d-flex justify-content-center pb-4'>
        <Pagination
        itemClass="page-item"
        linkClass="page-link"
        activePage={pagina}
        itemsCountPerPage={LIMIT}
        totalItemsCount={1820}
        onChange={handlePageChange}
        />
      </div>
    </div>

  )
}

export default App
